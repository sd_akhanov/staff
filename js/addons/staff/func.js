(function(_, $) {
    
    $(document).ready(function(){
        $(document).on('click','.mail_a', function(){
			var staff_id = $(this).attr('staff_id');
			fn_get_mail_staff_member(staff_id);
			return false;
		});
    });

    function fn_get_mail_staff_member(staff_id) {
        
        $.ceAjax('request', fn_url("staff.get_mail_staff"), {
			method: 'post',
            data: {
                staff_id: staff_id,
				result_ids: 'div_staff_'+staff_id,
            },
        });
    }

}(Tygh, Tygh.$));
