<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/


use Tygh\Navigation\LastView;


if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_get_staff_data($staff_id, &$auth, $lang_code = CART_LANGUAGE, $field_list = '', $get_add_pairs = true, $get_main_pair = true) 
{
	$staff = db_get_row("SELECT ?:staff.staff_id, ?:staff.firstname, ?:staff.lastname, ?:staff.email,?:staff.function,?:staff.user_id, ?:staff.priority FROM ?:staff WHERE ?:staff.staff_id = ?i  ",$staff_id);
	
	 // Get main image pair
	$staff['main_pair'] = fn_get_image_pairs($staff_id, 'staff', 'M', true, true, $lang_code);
	
	return $staff;
}

function fn_delete_staff($staff_id) { 
	$result = db_query("DELETE FROM ?:staff WHERE staff_id = ?i", $staff_id);
	
	return $result;
}

function fn_update_staff($staff_data, $staff_id = 0, $lang_code = CART_LANGUAGE)
{
	$staff_data['user_id'] = $staff_data['user_id']?(int)$staff_data['user_id']:null;
	$staff_data['priority'] = $staff_data['priority']?(int)$staff_data['priority']:null;
	
	// add new staff
	if (empty($staff_id)) {
		
		$staff_id = db_query("INSERT INTO ?:staff ?e", $staff_data);
	
	// update staff
	} else {
	   
		$arow = db_query("UPDATE ?:staff SET ?u WHERE staff_id = ?i", $staff_data, $staff_id);
	   
	}

	if ($staff_id) {
		
		// Update main images pair
		fn_attach_image_pairs('staff_main', 'staff', $staff_id, $lang_code);

	}
	
    return (int) $staff_id;
}

function fn_get_mail_staff($staff_id){
	$staff = db_get_row("SELECT coalesce(?:staff.email,?:users.email) as email FROM ?:staff LEFT JOIN ?:users ON ?:users.user_id = ?:staff.user_id WHERE ?:staff.staff_id = ?i  ",$staff_id);
	
	return $staff['email'];
}

function fn_get_all_staff() 
{
	$staff = db_get_array("SELECT ?:staff.staff_id, ?:staff.firstname, ?:staff.lastname, ?:staff.function,?:staff.user_id, ?:staff.priority FROM ?:staff order by coalesce(?:staff.priority,99999999)");
 
	$staff_ids = array();
	foreach($staff as $v) 
	{
		$staff_ids[] = $v['staff_id'];
	}

	$image_pairs = fn_get_image_pairs($staff_ids, 'staff', 'M', true, true, CART_LANGUAGE);
	foreach($staff as $k=>$v) {
		$temp = $image_pairs[$v['staff_id']];
		$staff[$k]['image_pair'] = $temp[key($temp)];
	}
	unset($image_pairs,$temp);
	
	return $staff;
}


function fn_get_staff($params, &$auth, $items_per_page = 0, $custom_view = '')
{
	

    // Init filter
    $_view = !empty($custom_view) ? $custom_view : 'users';
    $params = LastView::instance()->update($_view, $params);

    // Set default values to input params
    $default_params = array (
        'page' => 1,
        'items_per_page' => $items_per_page
    );

    $params = array_merge($default_params, $params);

    // Define fields that should be retrieved
    $fields = array(
        "?:staff.staff_id",
        "coalesce(?:staff.firstname,?:users.firstname) as firstname",
        "coalesce(?:staff.lastname,?:users.lastname) as lastname",
        "coalesce(?:staff.email,?:users.email) as email",
        "?:staff.function",
        "?:staff.user_id",
		"?:users.user_type"
       
    );

    // Define sort fields
    $sortings = array(
        'id' => "?:staff.staff_id",
        'email' => "?:staff.email",
        'name' => array("?:staff.lastname", "?:staff.firstname"),
        'function' => "?:staff.function",
        'user_id' => "?:staff.user_id",
       
    );

    if (isset($params['compact']) && $params['compact'] == 'Y') {
        $union_condition = ' OR ';
    } else {
        $union_condition = ' AND ';
    }

    $condition = $compact_fields = array();
    $join = $group = '';

    $group .= " GROUP BY ?:staff.staff_id";

	if (isset($params['name']) && fn_string_not_empty($params['name'])) {
        $arr = fn_explode(' ', $params['name']);
        foreach ($arr as $k => $v) {
            if (!fn_string_not_empty($v)) {
                unset($arr[$k]);
            }
        }
        if (sizeof($arr) == 2) {
            $condition['name'] = db_quote(" AND (?:staff.firstname LIKE ?l AND ?:staff.lastname LIKE ?l)",  "%".array_shift($arr)."%", "%".array_shift($arr)."%");
        } else {
            $condition['name'] = db_quote(" AND (?:staff.firstname LIKE ?l OR ?:staff.lastname LIKE ?l)", "%".trim($params['name'])."%", "%".trim($params['name'])."%");
        }
        $compact_fields[] = 'name';
    }
	
	if (isset($params['function']) && fn_string_not_empty($params['function'])) {
        $condition['function'] = db_quote(" $union_condition ?:staff.function LIKE ?l", "%".trim($params['function'])."%");
        $compact_fields[] = 'function';
    }
	
	if (isset($params['email']) && fn_string_not_empty($params['email'])) {
        $condition['email'] = db_quote(" $union_condition ?:staff.email LIKE ?l", "%".trim($params['email'])."%");
        $compact_fields[] = 'email';
    }
    


    $join .= db_quote(" LEFT JOIN ?:users ON ?:users.user_id = ?:staff.user_id");

    $sorting = db_sort($params, $sortings, 'name', 'asc');

    // Used for Extended search
    if (!empty($params['get_conditions'])) {
        return array($fields, $join, $condition);
    }

    // Paginate search results
    $limit = '';
    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field("SELECT COUNT(DISTINCT(?:staff.staff_id)) FROM ?:staff $join WHERE 1 ". implode(' ', $condition));
        $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
    }
	
    $staff = db_get_array("SELECT " . implode(', ', $fields) . " FROM ?:staff $join WHERE 1" . implode('', $condition) . " $group $sorting $limit");
	
    return array($staff, $params);
}