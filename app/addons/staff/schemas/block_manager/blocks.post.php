<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

$schema['staff'] = array (
	'content' => array(
		'staff_members' => array(
			'type' => 'function',
			'function' => array('fn_get_all_staff'),
		)
	),
	'cache' => array(
		'update_handlers' => array(
			'staff',
			'images_links'
		)
	),
    'templates' => array(
        'addons/staff/blocks/staff.tpl' => array(
			'settings' => array(
				'not_scroll_automatically' => array (
					'type' => 'checkbox',
					'default_value' => 'Y'
				),
				'scroll_per_page' =>  array (
					'type' => 'checkbox',
					'default_value' => 'N'
				),
				'speed' =>  array (
					'type' => 'input',
					'default_value' => 400
				),
				'pause_delay' =>  array (
					'type' => 'input',
					'default_value' => 3
				),
				'item_quantity' =>  array (
					'type' => 'input',
					'default_value' => 7
				),
				'thumbnail_width' =>  array (
					'type' => 'input',
					'default_value' => 180
				),
				'outside_navigation' => array (
					'type' => 'checkbox',
					'default_value' => 'N'
				)
			),
		),
    ),
    'wrappers' => 'blocks/wrappers',
);

return $schema;
