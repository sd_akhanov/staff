<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	
	if($mode == 'get_mail_staff') {
		$staff_member['staff_id'] = (int)$_REQUEST['staff_id'];
		$mail = fn_get_mail_staff($staff_member['staff_id']);
		
		Tygh::$app['view']->assign('staff_member', $staff_member);
		Tygh::$app['view']->assign('mail', $mail);
		Tygh::$app['view']->display('addons/staff/views/mail.tpl');
		exit;
	}
}
