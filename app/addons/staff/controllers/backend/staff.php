<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	//
    // Create/update product
    //
	if ($mode == 'update') {
		
		$staff_id = fn_update_staff($_REQUEST['staff'], $_REQUEST['staff_id'], DESCR_SL);
		return array(CONTROLLER_STATUS_OK, 'staff.update?staff_id=' . $staff_id);	
	}
	if ($mode == 'delete') {
		
        fn_delete_staff($_REQUEST['staff_id']);
        return array(CONTROLLER_STATUS_OK, 'staff.staff_members');
    }
   
}


if ($mode == 'staff_members') {
	
	
    list($staff, $search) = fn_get_staff($_REQUEST, $auth, Registry::get('settings.Appearance.admin_elements_per_page'));
	
    Tygh::$app['view']->assign('staff_members', $staff);
    Tygh::$app['view']->assign('search', $search);

   

} elseif ($mode == 'update' || $mode == 'add') {
	$staff = array();
	if(isset($_REQUEST['staff_id'])) {
		$staff = fn_get_staff_data($_REQUEST['staff_id'],$auth);
	}	
	
	Tygh::$app['view']->assign('staff', $staff);
}