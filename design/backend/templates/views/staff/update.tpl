{if $staff.staff_id}
	{assign var="id" value=$staff.staff_id}
{else}
	{assign var="id" value=0}
{/if}
<form name="staff_form" action="{""|fn_url}" method="post" class="form-horizontal form-edit form-table" enctype="multipart/form-data">
	<input type="hidden"  name="staff_id" value="{$id}" />
{capture name="mainbox"}
	
	<div class="control-group">
		<label class="control-label cm-required" for="elm_first_name">{__("first_name")}:</label>
		<div class="controls">
			<input type="text" name="staff[firstname]" id="elm_first_name" size="20" maxlength="128"  value="{$staff.firstname}" class="input-large" />
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="elm_last_name">{__("last_name")}:</label>
		<div class="controls">
			<input type="text" name="staff[lastname]" id="elm_last_name" size="20" maxlength="128"  value="{$staff.lastname}" class="input-large" />
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="elm_last_email">{__("email")}:</label>
		<div class="controls">
			<input type="text" name="staff[email]" id="elm_last_email" size="20" maxlength="158"  value="{$staff.email}" class="input-large" />
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="elm_function">{__("function")}:</label>
		<div class="controls">
			<textarea id="elm_function" name="staff[function]" cols="55" rows="2" class="input-large">{$staff.function}</textarea>
		</div>
	</div>
	
	<div class="control-group">
                        <label class="control-label">{__("images")}:</label>
                        <div class="controls">
                            {include file="views/staff/components/attach_images.tpl" image_name="staff_main" image_object_type="staff" image_pair=$staff.main_pair icon_text=__("text_staff_thumbnail") detailed_text=__("text_staff_detailed_image") no_thumbnail=0}
                        </div>
                    </div>
	
	<div class="control-group">
		<label class="control-label" for="elm_user_id">{__("user_id")}:</label>
		<div class="controls">
			<input type="text" name="staff[user_id]" id="elm_user_id" size="20" maxlength="128"  value="{$staff.user_id}" class="input-large" />
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="elm_user_id">{__("priority")}:</label>
		<div class="controls">
			<input type="text" name="staff[priority]" id="elm_user_id" size="20" maxlength="128"  value="{$staff.priority}" class="input-large" />
		</div>
	</div>
{/capture}

{capture name="buttons"}
	{include file="buttons/save_cancel.tpl" but_role="submit-link" but_name="dispatch[staff.update]" but_target_form="staff_update_form" save=$id}
{/capture}




{include file="common/mainbox.tpl" title=$_title content=$smarty.capture.mainbox buttons=$smarty.capture.buttons sidebar=$smarty.capture.sidebar}
</form>
