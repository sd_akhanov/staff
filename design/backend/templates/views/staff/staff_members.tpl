

{capture name="mainbox"}

{assign var="c_icon" value="<i class=\"exicon-`$search.sort_order_rev`\"></i>"}
{assign var="c_dummy" value="<i class=\"exicon-dummy\"></i>"}

<form action="{""|fn_url}" method="post" name="userlist_form" id="userlist_form" class="{if $runtime.company_id && !"ULTIMATE"|fn_allowed_for}cm-hide-inputs{/if}">
<input type="hidden" name="fake" value="1" />


{include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id=$smarty.request.content_id}

{assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}

{assign var="rev" value=$smarty.request.content_id|default:"pagination_contents"}


{if $staff_members}
<table width="100%" class="table table-middle">
<thead>
<tr>
   
    <th width="3%" class="nowrap"><a class="cm-ajax" href="{"`$c_url`&sort_by=id&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("id")}{if $search.sort_by == "id"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
	
    <th width="18%"><a class="cm-ajax" href="{"`$c_url`&sort_by=name&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("person_name")}{if $search.sort_by == "name"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
	<th width="16%"><a class="cm-ajax" href="{"`$c_url`&sort_by=function&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("function")}{if $search.sort_by == "function"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    <th width="20%"><a class="cm-ajax" href="{"`$c_url`&sort_by=email&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("email")}{if $search.sort_by == "email"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
	
    
    <th><a class="cm-ajax" href="{"`$c_url`&sort_by=user_id&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("user_id")}{if $search.sort_by == "user_id"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
    {hook name="profiles:manage_header"}{/hook}
    <th class="right">&nbsp;</th>
   

</tr>
</thead>
{foreach from=$staff_members item=staff_member}


<tr>

    <td><a class="row-status" href="{"staff_member.update?staff_memberid=`$staff_member.staff_memberid`"|fn_url}">{$staff_member.staff_id}</a></td>
    <td class="row-status"><a href="{"staff.update?staff_id=`$staff_member.staff_id`"|fn_url}">{$staff_member.lastname} {$staff_member.firstname}</a></td>
    <td>{$staff_member.function}</td>
	<td><a class="row-status" href="mailto:{$staff_member.email|escape:url}">{$staff_member.email}</a></td>
	
    
    <td><a class="row-status" href="{"profiles.update?user_id=`$staff_member.user_id`&user_type=`$staff_member.user_type`"|fn_url}">{$staff_member.user_id}</a></td>
    
    <td class="right nowrap">
        {capture name="tools_list"}
            

           
            <li>{btn type="list" text=__("edit") href="staff.update&staff_id=`$staff_member.staff_id`"}</li>
			<li>{btn type="list" text=__("delete") class="cm-confirm" href="staff.delete?staff_id=`$staff_member.staff_id`" method="POST"}</li>
            
			
            
        {/capture}
        <div class="hidden-tools">
            {dropdown content=$smarty.capture.tools_list}
        </div>
    </td>
   
</tr>
{/foreach}
</table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

{include file="common/pagination.tpl" div_id=$smarty.request.content_id}


</form>
{/capture}

{capture name="adv_buttons"}
    
	{assign var="_title" value=__("staff")}
   
	    
	<a class="btn cm-tooltip" href="{"staff.add"|fn_url}" title="{__("add_staff")}"><i class="icon-plus"></i></a>
    
{/capture}

{capture name="sidebar"}
    {include file="views/staff/components/staff_search_form.tpl" dispatch="staff.manage"}
{/capture}

{include file="common/mainbox.tpl" title=$_title content=$smarty.capture.mainbox sidebar=$smarty.capture.sidebar adv_buttons=$smarty.capture.adv_buttons buttons=$smarty.capture.buttons content_id="manage_users"}